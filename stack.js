// Your implementation goes here

// Constructor
function Stack() {

    this.stackdata = Array.from(arguments);
    this.size = this.stackdata.length;
}
// push Method add new element is array 
Stack.prototype.push = function (element) {
    this.stackdata.push(element);
    this.newSize();
}
// newSize Method return latest length of the array 
Stack.prototype.newSize = function () {
    this.size = this.stackdata.length;
}
// pOp Method remove the top element of the array
Stack.prototype.pop = function () {
    if (this.stackdata.length === 0)
        return null;
    return this.stackdata.pop();
}
// isEmpty Method check list empty or not
Stack.prototype.isEmpty = function () {
    return this.stackdata.length === 0;
}
// peek Method return the top element of the array
Stack.prototype.peek = function () {
    return this.stackdata[this.stackdata.length - 1];
}
// swap Method swap the last 2 element of the array
Stack.prototype.swap = function () {
    let arraydata = this.stackdata[this.stackdata.length - 1];
    let temp = this.stackdata[this.stackdata.length - 2];
    this.stackdata[this.stackdata.length - 1] = temp;
    this.stackdata[this.stackdata.length - 2] = arraydata;
}

module.exports = Stack;
